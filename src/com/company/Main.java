package com.company;

import java.util.Scanner;
class Main {
    public static void main(String[] args) {
        boolean finish = false;
        Quiz quiz = new QuizImpl();
        Scanner scanner = new Scanner(System.in);
        int digit = 0;
        for (int counter = 1; ; counter++) {
            while(finish == false){
                System.out.println("Podaj liczbę z zakresu 0-1000: ");
                digit = scanner.nextInt();
                try {
                    quiz.isCorrectValue(digit);
                    System.out.println("Trafiona proba!!! Szukana liczba to: "
                            + digit + " Ilosc prob: " + counter);
                    finish = true;
                    break;
                } catch (Quiz.ParamTooLarge paramTooLarge) {
                    System.out.println("Argument za duzy!!!");
                    finish = false;
                } catch (Quiz.ParamTooSmall paramTooSmall) {
                    System.out.println("Argument za maly!!!");
                    finish = false;
                }break;
            }continue;
        }
    }
}
